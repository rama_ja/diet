<?php

namespace App\Http\Requests\Api\Resturant_App\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class StoreCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'                             => 'required|string',
            'minimum_price'                    => 'required|integer|gt:0',
            'discount_amount'                  => 'required|integer|gt:0',
            'discount_type'                    => 'required|integer',
            'start_date'                       => 'required|date',
            'end_date'                         => 'required|date',
        ];

    }
}
