<?php

namespace App\Http\Requests\Api\Resturant_App\Resturant;

use Illuminate\Foundation\Http\FormRequest;

class UpdateResturantWorkingHoursRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'day_id'           => 'required|integer',
            'opening_hour'     => 'required',
            'closing_hour'     => 'required',
        ];
    }
}
