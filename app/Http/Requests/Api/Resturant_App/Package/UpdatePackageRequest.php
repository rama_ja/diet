<?php

namespace App\Http\Requests\Api\Resturant_App\Package;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'                              => 'required|string',
            'name_en'                              => 'required|string',
            'price'                                => 'required|numeric|gt:0',
            'description_ar'                       => 'required|string',
            'description_en'                       => 'required|string',
            'total_calories'                       => 'required|numeric',
            'carbohydrate'                         => 'required|numeric',
            'protein'                              => 'required|numeric',
            'fats'                                 => 'required|numeric',
            'image_upload_type'                    => 'required|integer',//0 no update , 1 upload file , 2 choose from gallery
            'main_image_file'                      => 'required_if:image_upload_type,1|image|mimes:jpg,jpeg,png,gif,webp',
            'main_image_number'                    => 'required_if:image_upload_type,2|integer',
            'status'                               => 'required|integer',
            'package_details.*.day_id'             => 'integer',
            'package_details.*.delivery_time'      => 'required_if:deleted_day,1',
            'package_details.*.description_ar'     => 'required_if:deleted_day,1',
            'package_details.*.description_en'     => 'required_if:deleted_day,1',
        ];
    }
}
