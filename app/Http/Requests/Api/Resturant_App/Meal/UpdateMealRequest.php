<?php

namespace App\Http\Requests\Api\Resturant_App\Meal;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'                         => 'required|string',
            'name_en'                         => 'required|string',
            'price'                           => 'required|numeric|gt:0',
            'description_ar'                  => 'required|string',
            'description_en'                  => 'required|string',
            'total_calories'                  => 'required|numeric',
            'carbohydrate'                    => 'required|numeric',
            'protein'                         => 'required|numeric',
            'fats'                            => 'required|numeric',
            'image_upload_type'               => 'required|integer',//0 no update , 1 upload file , 2 choose from gallery
            'main_image_file'                 => 'required_if:image_upload_type,1|image|mimes:jpg,jpeg,png,gif,webp',
            'main_image_number'               => 'required_if:image_upload_type,2|integer',
            'status'                          => 'required|integer',
            'has_discount'                    => 'required|integer',
            'discount_type'                   => 'required_if:has_discount,1|integer',
            'discount_value'                  => 'required_if:has_discount,1|string',
            'discount_start_date'             => 'required_if:has_discount,1|date',
            'discount_end_date'               => 'required_if:has_discount,1|date',
            'extras.*'
        ];
    }
}
