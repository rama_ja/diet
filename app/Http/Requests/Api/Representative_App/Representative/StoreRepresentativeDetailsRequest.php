<?php

namespace App\Http\Requests\Api\Representative_App\Representative;

use Illuminate\Foundation\Http\FormRequest;

class StoreRepresentativeDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // The 2 Satage Data
            'vehicle_type'                    => 'required|string',
            'vehicle_brand'                   => 'required|string',
            'type'                            => 'required|string',
            'vehicle_model'                   => 'required|string',
            'vehicle_number'                  => 'required|string',
            'license_image'                   => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'form_image'                      => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'images'                          => 'required|array',
            'images.*'                        => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ];

    }
}
