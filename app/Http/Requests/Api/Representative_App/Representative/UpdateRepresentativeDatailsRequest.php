<?php

namespace App\Http\Requests\Api\Representative_App\Representative;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRepresentativeDatailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                            => 'required|string',
            'email'                           => 'nullable|email',
            'mobile'                          => 'required|string',
            'city_id'                         => 'required|integer',
            'personal_image'                  => 'nullable|image|mimes:jpg,jpeg,png,gif,webp',
            'vehicle_type'                    => 'required|string',
            'vehicle_brand'                   => 'required|string',
            'type'                            => 'required|string',
            'vehicle_model'                   => 'required|string',
            'vehicle_number'                  => 'required|string',
            'license_image'                   => 'nullable|image|mimes:jpg,jpeg,png,gif,webp',
            'form_image'                      => 'nullable|image|mimes:jpg,jpeg,png,gif,webp',
            'images'                          => 'nullable|array',
            'images.*'                        => 'nullable|image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}
