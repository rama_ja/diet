<?php

namespace App\Http\Requests\Api\Client_App\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                            => 'required|string',
            'email'                           => 'required|email|unique:clients,email',
            'mobile'                          => 'required|string|unique:clients,mobile',
            'password'                        => ['required', 'confirmed', Password::min(8)->letters()->mixedCase()->numbers()->symbols()],
            'city_id'                         => 'required|integer',
            'personal_image'                  => 'nullable|image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}
