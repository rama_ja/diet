<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Resturant_App\Package\PackageImagesResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\PackageImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class PackageImageController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index(Request $request)
    {
        $package_images = PackageImage::where('package_id', $request->package_id)->get();
        return $this->apiResponse(PackageImagesResource::collection($package_images), 'All Images for this Package', 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'package_id'   => 'required',
            'images'       => 'required|array|max:10',
            'images.*'     => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $final_array = [];
        if ($request->file('images')) {
            foreach ($request->file('images') as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Packages/PackageImages', 400, 400);
                $new_image = PackageImage::create(['package_id' => $request->package_id, 'image' => $image_name_DataBase]);
                array_push($final_array, $new_image);
            }
        }

        return $this->apiResponse(PackageImagesResource::collection($final_array), 'The Image has been created successfully', 200);
    }

    public function destroy($id)
    {
        $package_image = PackageImage::find($id);
        (File::exists($package_image->image)) ? File::delete($package_image->image) : Null;
        $package_image->delete();
        return $this->apiResponse(null, 'The package image has been deleted successfully', 200);
    }
}
