<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Coupon\StoreCouponRequest;
use App\Http\Resources\Api\Resturant_App\Coupon\CouponResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    use  ApiResponseTrait;

    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $coupons = Coupon::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(CouponResource::collection($coupons), 'All coupons of this restaurant', 200);
    }
    public function store(StoreCouponRequest $request)
    {
        $resturant = $request->user('resturant_api');
        $coupon = Coupon::create([
            'resturant_id'      => $resturant->id,
            'code'              => $request->code,
            'minimum_price'     => $request->minimum_price,
            'discount_amount'   => $request->discount_amount,
            'discount_type'     => $request->discount_type,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date
        ]);

        return $this->apiResponse(new CouponResource($coupon), 'The coupon has been created successfully', 200);
    }


    public function destroy($id)
    {
        $coupon=Coupon::find($id);
        $coupon->delete();
        return $this->apiResponse(null, 'The coupon has been deleted successfully', 200);
    }
}
