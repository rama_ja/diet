<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantImagesResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Resturant;
use App\Models\ResturantImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ResturantImageController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $meal_images = ResturantImage::where('resturant_id', $resturant->id)->get();
        return $this->apiResponse(ResturantImagesResource::collection($meal_images), 'All Images for this Resturant', 200);
    }

    public function store(Request $request)
    {
        $resturant = $request->user('resturant_api');

        $resturant->update([
            'status'            => '2',
        ]);
        $validator = Validator::make($request->all(), [
            'images'    => 'required|array|max:10',
            'images.*'  => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $final_array = [];

        if ($request->file('images')) {
            foreach ($request->file('images') as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Resturants/ResturantImages', 400, 400);
                $new_image = ResturantImage::create(['resturant_id' => $resturant->id, 'image' => $image_name_DataBase]);
                array_push($final_array, $new_image);
            }
        }

        return $this->apiResponse(ResturantImagesResource::collection($final_array), 'The Image has been created successfully', 200);
    }

    public function destroy($id)
    {
        $resturant_image = ResturantImage::find($id);
        (File::exists($resturant_image->image)) ? File::delete($resturant_image->image) : Null;
        $resturant_image->delete();
        return $this->apiResponse(null, 'The resturant image has been deleted successfully', 200);
    }
}
