<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Resturant\StoreResturantWorkingHoursRequest;
use App\Http\Requests\Api\Resturant_App\Resturant\UpdateResturantWorkingHoursRequest;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Resturant;
use App\Models\WorkingHour;
use Illuminate\Http\Request;

class WorkingHoursController extends Controller
{
    use ApiResponseTrait;


    public function store(StoreResturantWorkingHoursRequest $request)
    {
        $resturant = $request->user('resturant_api');

        $resturant->workingHours()->delete();

        foreach ($request->input('working_days') as $working_day) {
            WorkingHour::create([
                'resturant_id' => $resturant->id,
                'day_id'       => $working_day['day_id'],
                'opening_hour' => $working_day['opening_hour'],
                'closing_hour' => $working_day['closing_hour'],
            ]);
        }


        return $this->apiResponse(new ResturantResource($resturant), 'The working hours have been added successfully', 200);
    }
    public function update(UpdateResturantWorkingHoursRequest $request,$id)
    {
        $resturant = $request->user('resturant_api');
        $work=WorkingHour::find($id);
        $work->update([
                'resturant_id' => $resturant->id,
                'day_id'       => $request->day_id,
                'opening_hour' => $request->opening_hour,
                'closing_hour' => $request->closing_hour,
            ]);

        return $this->apiResponse(new ResturantResource($resturant), 'The working hours have been added successfully', 200);

    }
    public function destroy(Request $request,$id)
    {
        $resturant = $request->user('resturant_api');
        $work=WorkingHour::find($id);
        $work->delete();

        return $this->apiResponse(new ResturantResource($resturant), 'The working hours have been deleted successfully', 200);

    }
}
