<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Resturant_App\Resturant\StoreResturantDetailsRequest;
use App\Http\Requests\Api\Resturant_App\Resturant\StoreResturantDocumentsRequest;
use App\Http\Requests\Api\Resturant_App\Resturant\StoreResturantRequest;
use App\Http\Requests\Api\Resturant_App\Resturant\StoreResturantWorkingHoursRequest;
use App\Http\Requests\Api\Resturant_App\Resturant\UpdateResturantDetailsRequest;
use App\Http\Resources\Api\Resturant_App\City\CityResource;
use App\Http\Resources\Api\Resturant_App\Resturant\DayResource;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\City;
use App\Models\Day;
use App\Models\Resturant;
use App\Models\ResturantImage;
use App\Models\WorkingHour;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class AuthController extends Controller
{
    use ApiResponseTrait, SaveImageTrait;

    public function __construct()
    {
        $this->middleware('auth:resturant_api', ['except' => ['login', 'register', 'getCities']]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'    => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$token = auth('resturant_api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }

    public function getCities()
    {
        $cities = City::paginate(20);;
        $days = Day::paginate(20);;
        $data = [
            'cities' => CityResource::collection($cities),
            'days' => DayResource::collection($days)
        ];
        return $this->apiResponse($data, 'The Required Cities & Days for restaurant registration', 200);
    }

    public function register(StoreResturantRequest $request)
    {
        $resturant = Resturant::create([
            'email'             => $request->email,
            'mobile'            => $request->mobile,
            'password'          => Hash::make($request->password),
            'status'            => '1',
            'language'          => $request->language,
            'ar' => [
                'name'          => $request->name_ar,
            ],
            'en' => [
                'name'          => $request->name_en,
            ]
        ]);

        $token = auth('resturant_api')->login($resturant);

        return response()->json([
            'message'      => 'Restaurant successfully registered',
            'access_token' => $token,
            'resturant'    => new ResturantResource($resturant)
        ], 201);
    }

    public function addRestaurantDetails(StoreResturantDetailsRequest $request)
    {
        $resturant = $request->user('resturant_api');

        $update_resturant = Resturant::find($resturant->id);
        $update_resturant->update([
            'city_id'           => $request->city_id,
            'latitude'          => $request->latitude,
            'longitude'         => $request->longitude,
            'logo'              => $this->saveImage($request->file('logo'), 'Images/Resturants/Logos', 400, 400),
            'main_image'        => $this->saveImage($request->file('main_image'), 'Images/Resturants/MainImages', 400, 400),
            'ar' => ['description'   => $request->description_ar],
            'en' => ['description'   => $request->description_en]
        ]);

        $Images = $request->file('images');
        if ($Images) {
            foreach ($Images as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Resturants/ResturantImages', 400, 400);
                ResturantImage::create(['resturant_id' => $resturant->id, 'image' => $image_name_DataBase]);
            }
        }

        return $this->apiResponse(new ResturantResource($update_resturant), 'Restaurant details have been added successfully', 200);
    }

    public function updateRestaurantDetails(UpdateResturantDetailsRequest $request)
    {
        $resturant = $request->user('resturant_api');

        $update_resturant = Resturant::find($resturant->id);
        $update_resturant->update([
            'city_id'           => $request->city_id,
            'latitude'          => $request->latitude,
            'longitude'         => $request->longitude,
            'status'            => '2',
            'ar'  => [
                'name'        => $request->name_ar,
                'description' => $request->description_ar,
            ],
            'en'  => [
                'name'        => $request->name_en,
                'description' => $request->description_en,
            ]
        ]);

        if($request->file('logo')){
            $update_resturant->update([
                'logo'              => $this->saveImage($request->file('logo'), 'Images/Resturants/Logos', 400, 400)
            ]);
        }
        if($request->file('main_image')){
            $update_resturant->update([
                'main_image'        => $this->saveImage($request->file('main_image'), 'Images/Resturants/MainImages', 400, 400)
            ]);
        }
        $Images = $request->file('images');
        if ($Images) {
            foreach ($Images as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Resturants/ResturantImages', 400, 400);
                ResturantImage::create(['resturant_id' => $resturant->id, 'image' => $image_name_DataBase]);
            }
        }

        return $this->apiResponse(new ResturantResource($update_resturant), 'Restaurant details have been updated successfully', 200);
    }

    public function uploadDocuments(StoreResturantDocumentsRequest $request)
    {
        $resturant = $request->user('resturant_api');

        $update_resturant = Resturant::find($resturant->id);
        $update_resturant->update([
            'status'            => '2',
            'commercial_record' => $this->saveImage($request->file('commercial_record'), 'Images/Resturants/CommercialRecords', 400, 400),
            'municipal_licence' => $this->saveImage($request->file('municipal_licence'), 'Images/Resturants/MunicipalLicences', 400, 400),
        ]);

        return $this->apiResponse(new ResturantResource($update_resturant), 'The documents have been successfully uploaded', 200);
    }

//    public function setWorkingHours(StoreResturantWorkingHoursRequest $request)
//    {
//        $resturant = $request->user('resturant_api');
//
//        foreach ($request->input('working_days') as $working_day) {
//            WorkingHour::create([
//                'resturant_id' => $resturant->id,
//                'day_id'       => $working_day['day_id'],
//                'opening_hour' => $working_day['opening_hour'],
//                'closing_hour' => $working_day['closing_hour'],
//            ]);
//        }
//
//        return $this->apiResponse(new ResturantResource($resturant), 'The working hours have been set successfully', 200);
//    }

    public function logout(Request $request)
    {
        if ($request->token) {
            $tokens = ClientDeviceToken::where('token', $request->token)->first();
            $tokens->delete();
        }
        auth('resturant_api')->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    public function refresh()
    {
        return $this->createNewToken(auth('resturant_api')->refresh());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth('resturant_api')->factory()->getTTL() * 60,
            'resturant'     => new ResturantResource(auth('resturant_api')->user())
        ]);
    }
}
