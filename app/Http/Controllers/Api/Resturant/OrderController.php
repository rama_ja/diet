<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Client_App\Order\OrderResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use  ApiResponseTrait;

    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $orders = Order::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(OrderResource::collection($orders), 'All orders of this restaurant', 200);
    }
    public function changeStatus(Request $request, $id)
    {
        $order=Order::find($id)->update(['status' => $request->status]);
        return $this->apiResponse(null, 'Order status changed successfully', 200);
    }
}
