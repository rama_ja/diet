<?php

namespace App\Http\Controllers\Api\Resturant;

use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Resturant_App\Representative\StoreRepresentativeRequest;
use App\Http\Requests\Api\Resturant_App\Representative\UpdateRepresentativeRequest;
use App\Http\Resources\Api\Resturant_App\City\CityResource;
use App\Http\Resources\Api\Resturant_App\Representative\RepresentativeResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\City;
use App\Models\Representative;
use App\Models\Resturant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RepresentativeController extends Controller
{

    use SaveImageTrait, ApiResponseTrait;
    public function index(Request $request)
    {
        $resturant = $request->user('resturant_api');
        $representative = Representative::where('resturant_id', $resturant->id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(RepresentativeResource::collection($representative), 'All representatives of this restaurant', 200);
    }

    public function getCities()
    {
        $cities=City::paginate(20);
        return $this->apiResponse(CityResource::collection($cities), 'The Required Cities', 200);
    }


    public function store(StoreRepresentativeRequest $request)
    {
        $resturant = $request->user('resturant_api');

            $representative = Representative::create([
                'resturant_id'     => $resturant->id,
                'name'             => $request->name,
                'email'            => $request->email,
                'password'         => Hash::make($request->password),
                'mobile'           => $request->mobile,
                'city_id'          => $request->city_id,
                'vehicle_type'     => $request->vehicle_type,
                'vehicle_brand'    => $request->vehicle_brand,
                'type'             => $request->type,
                'vehicle_model'    => $request->vehicle_model,
                'license_image'    => $this->saveImage($request->file('license_image'), 'Images/Representatives/LicenseImage', 400, 400),
                'form_image'       => $this->saveImage($request->file('form_image'), 'Images/Representatives/FormImage', 400, 400)
            ]);
        if($request->file('personal_image')) {
            $representative->update([
                    'personal_image' => $this->saveImage($request->file('personal_image'), 'Images/Representatives/PersonalImage', 400, 400)
           ]);
        }


        return $this->apiResponse(new RepresentativeResource($representative), 'The representative has been created successfully', 200);
    }


    public function update(UpdateRepresentativeRequest $request, $id)
    {
        $resturant = $request->user('resturant_api');
        $representative = Representative::find($id);

        $representative->update([
            'resturant_id'    => $resturant->id,
            'name'            => $request->name,
            'email'           => $request->email,
            'mobile'          => $request->mobile,
            'city_id'         => $request->city_id,
            'vehicle_type'    => $request->vehicle_type,
            'vehicle_brand'   => $request->vehicle_brand,
            'type'            => $request->type,
            'vehicle_model'   => $request->vehicle_model
        ]);

        if($request->file('personal_image')){
            $representative->update([
                'personal_image'  =>  $this->updateImage($representative, $request, 'personal_image', 'Images/Representatives/PersonalImage', 400, 400)
            ]);
        }
        if($request->file('license_image')){
            $representative->update([
                'license_image'   =>  $this->updateImage($representative, $request, 'license_image', 'Images/Representatives/LicenseImage', 400, 400)
            ]);
        }
        if($request->file('form_image')){
            $representative->update([
                'form_image'      =>  $this->updateImage($representative, $request, 'form_image', 'Images/Representatives/FormImage', 400, 400)
            ]);
        }

        return $this->apiResponse(new RepresentativeResource($representative), 'The representative has been updated successfully', 200);
    }

    public function destroy($id)
    {
        $representative = Representative::find($id);
        (File::exists($representative->personal_image)) ? File::delete($representative->personal_image) : Null;
        (File::exists($representative->license_image)) ? File::delete($representative->license_image) : Null;
        (File::exists($representative->form_image)) ? File::delete($representative->form_image) : Null;
        $representative->delete();
        return $this->apiResponse(null, 'The representative has been deleted successfully', 200);
    }

}
