<?php

namespace App\Http\Controllers\Api\Representative;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Client\UpdatePasswordRequest;
use App\Http\Requests\Api\Representative_App\Representative\StoreAuthRepresentativeRequest;
use App\Http\Requests\Api\Representative_App\Representative\StoreRepresentativeDetailsRequest;
use App\Http\Requests\Api\Representative_App\Representative\UpdateRepresentativeDatailsRequest;
use App\Http\Resources\Api\Resturant_App\Representative\RepresentativeResource;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Representative;
use App\Models\VehicleImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use SaveImageTrait,ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:representative_api', ['except' => ['login', 'register']]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'    => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$token = auth('representative_api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }

    public function register(StoreAuthRepresentativeRequest $request)
    {
        $representative = Representative::create([
            'name'              => $request->name,
            'email'             => $request->email,
            'mobile'            => $request->mobile,
            'password'          => Hash::make($request->password),
            'city_id'           => $request->city_id,
            'status'            =>'2'
        ]);
        if($request->file('personal_image')) {
            $representative->update([
                'personal_image' => $this->saveImage($request->file('personal_image'), 'Images/Representatives/PersonalImage', 400, 400)
            ]);
        }


        $token = auth('representative_api')->login($representative);

        return response()->json([
            'message'      => 'Representative successfully registered',
            'access_token' => $token,
            'representative'    => new RepresentativeResource($representative)
        ], 201);
    }

    public function addRepresentativeDetails(StoreRepresentativeDetailsRequest $request)
    {
        $representative = $request->user('representative_api');

        $representative->update([
            'vehicle_type'           => $request->vehicle_type,
            'vehicle_brand'          => $request->vehicle_brand,
            'type'                   => $request->type,
            'vehicle_model'          => $request->vehicle_model,
            'vehicle_number'         => $request->vehicle_number,
            'license_image'          => $this->saveImage($request->file('license_image'), 'Images/Representatives/LicenseImage', 400, 400),
            'form_image'             => $this->saveImage($request->file('form_image'), 'Images/Representatives/FormImage', 400, 400)
        ]);

        $Images = $request->file('images');
        if ($Images) {
            foreach ($Images as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Representatives/VehicleImages', 400, 400);
                VehicleImage::create(['representative_id' => $representative->id, 'image' => $image_name_DataBase]);
            }
        }

        return $this->apiResponse(new RepresentativeResource($representative), 'Representative details have been added successfully', 200);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $representative = $request->user('representative_api');
        if (!auth('representative_api')->attempt(['mobile'=>$representative->mobile,'password'=>$request->old_password])) {
            return $this->apiResponse(null, 'The old password is incorrect', 200);
        }
        else{
            $representative->update([
                'password'          => Hash::make($request->password)
            ]);

            return $this->apiResponse(new RepresentativeResource($representative), 'The password has been updated successfully', 200);
        }


    }
    public function updateRepresentativeInfo(UpdateRepresentativeDatailsRequest $request)
    {
        $representative = $request->user('representative_api');

        $representative->update([
            'name'                   => $request->name,
            'email'                  => $request->email,
            'mobile'                 => $request->mobile,
            'city_id'                => $request->city_id,
            'vehicle_type'           => $request->vehicle_type,
            'vehicle_brand'          => $request->vehicle_brand,
            'type'                   => $request->type,
            'vehicle_model'          => $request->vehicle_model,
            'vehicle_number'         => $request->vehicle_number
        ]);

        if($request->file('personal_image')){
            $representative->update([
                'personal_image'  =>  $this->updateImage($representative, $request, 'personal_image', 'Images/Representatives/PersonalImage', 400, 400)
            ]);
        }
        if($request->file('license_image')){
            $representative->update([
                'license_image'   =>  $this->updateImage($representative, $request, 'license_image', 'Images/Representatives/LicenseImage', 400, 400)
            ]);
        }
        if($request->file('form_image')){
            $representative->update([
                'form_image'      =>  $this->updateImage($representative, $request, 'form_image', 'Images/Representatives/FormImage', 400, 400)
            ]);
        }
        $Images = $request->file('images');
        if ($Images) {
            foreach ($Images as $Image) {
                $image_name_DataBase = $this->saveImages($Image, 'Images/Representatives/VehicleImages', 400, 400);
                VehicleImage::create(['representative_id' => $representative->id, 'image' => $image_name_DataBase]);
            }
        }
        return $this->apiResponse(new RepresentativeResource($representative), 'The representative information has been updated successfully', 200);

    }

    public function destroyVehicleImage($id)
    {
        $vehicle_image = VehicleImage::find($id);
        (File::exists($vehicle_image->image)) ? File::delete($vehicle_image->image) : Null;
        $vehicle_image->delete();
        return $this->apiResponse(null, 'The vehicle image has been deleted successfully', 200);
    }

    public function logout(Request $request)
    {
        if ($request->token) {
            $tokens = ClientDeviceToken::where('token', $request->token)->first();
            $tokens->delete();
        }
        auth('representative_api')->logout();
        return response()->json(['message' => 'Representative successfully signed out']);
    }

    public function refresh()
    {
        return $this->createNewToken(auth('representative_api')->refresh());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth('representative_api')->factory()->getTTL() * 60,
            'representative'     => new RepresentativeResource(auth('representative_api')->user())
        ]);
    }

}
