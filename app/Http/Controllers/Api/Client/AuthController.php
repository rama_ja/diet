<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Client\StoreClientRequest;
use App\Http\Requests\Api\Client_App\Client\UpdateClientRequest;
use App\Http\Requests\Api\Client_App\Client\UpdatePasswordRequest;
use App\Http\Resources\Api\Client_App\Client\ClientResource;
use App\Http\Resources\Api\Resturant_App\City\CityResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\City;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use SaveImageTrait,ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:client_api', ['except' => ['login', 'register','getCities']]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'    => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$token = auth('client_api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }
    public function register(StoreClientRequest $request)
    {
        if($request->file('personal_image')){
            $client = Client::create([
                'name'              => $request->name,
                'email'             => $request->email,
                'mobile'            => $request->mobile,
                'password'          => Hash::make($request->password),
                'city_id'           => $request->city_id,
                'personal_image'  => $this->saveImage($request->file('personal_image'), 'Images/Clients', 400, 400),
            ]);
        }else{
            $client = Client::create([
                'name'              => $request->name,
                'email'             => $request->email,
                'mobile'            => $request->mobile,
                'password'          => Hash::make($request->password),
                'city_id'           => $request->city_id
            ]);
        }


        $token = auth('client_api')->login($client);

        return response()->json([
            'message'      => 'Client successfully registered',
            'access_token' => $token,
            'client'    => new ClientResource($client)
        ], 201);
    }
    public function getCities()
    {
        $cities=City::paginate(20);
        return $this->apiResponse(CityResource::collection($cities), 'The Required Cities', 200);
    }

    public function updateClientInfo(UpdateClientRequest $request)
    {
        $client = $request->user('client_api');


        $client->update([
            'name'              => $request->name,
            'email'             => $request->email,
            'mobile'            => $request->mobile,
            'city_id'           => $request->city_id,
           ]);

        if($request->file('personal_image')) {
            $client->update([
                'personal_image'    =>  $this->updateImage($client, $request, 'personal_image', 'Images/Clients', 400, 400)
            ]);
        }

        return $this->apiResponse(new ClientResource($client), 'The client has been updated successfully', 200);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $client = $request->user('client_api');
        if (!auth('client_api')->attempt(['mobile'=>$client->mobile,'password'=>$request->old_password])) {
            return $this->apiResponse(null, 'The old password is incorrect', 200);
        }
        else{
            $client->update([
                'password'          => Hash::make($request->password)
            ]);

            return $this->apiResponse(new ClientResource($client), 'The password has been updated successfully', 200);
        }


    }
    public function logout(Request $request)
    {
        if ($request->token) {
            $tokens = ClientDeviceToken::where('token', $request->token)->first();
            $tokens->delete();
        }
        auth('client_api')->logout();
        return response()->json(['message' => 'Client successfully signed out']);
    }

    public function refresh()
    {
        return $this->createNewToken(auth('client_api')->refresh());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth('client_api')->factory()->getTTL() * 60,
            'client'     => new ClientResource(auth('client_api')->user())
        ]);
    }

}
