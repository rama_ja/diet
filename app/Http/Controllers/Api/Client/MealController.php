<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Client_App\Package\StoreRateRequest;

use App\Http\Resources\Api\Client_App\Meal\MealRatingResource;
use App\Http\Resources\Api\Resturant_App\Meal\MealResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Meal;
use App\Models\MealFavorite;
use App\Models\MealRating;
use App\Models\Resturant;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MealController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function mealsForResturant(Request $request,$id)
    {
        $client = $request->user('client_api');
        $meals = Meal::where('resturant_id', $id)->orderBy('created_at', 'DESC')->paginate(20);
        return $this->apiResponse(MealResource::collection($meals), 'All meals for this resturant ', 200);
    }

    public function addToFavorite(Request $request,$id){
        $client = $request->user('client_api');
        $meal=Meal::find($id);

        $old_favorite=MealFavorite::where('meal_id',$meal->id)
            ->where('client_id',$client->id)->first();
        if($old_favorite){
            return $this->apiResponse(null, 'This meal is already added to favorite', 200);
        }

        $client->meal_favorites()->attach($meal->id);

        return $this->apiResponse(null, 'Meal successfully added to Favorite', 200);
    }

    public function removeFromFavorite(Request $request,$id){
        $client = $request->user('client_api');
        $meal=Meal::find($id);

        $client->meal_favorites()->detach($meal->id);


        return $this->apiResponse(null, 'meal successfully removed from Favorite', 200);
    }

    public function addEvaluation(StoreRateRequest $request,$id){
        $client = $request->user('client_api');
        $meal=Meal::find($id);
        $evaluation=MealRating::create([
            'meal_id'            =>$meal->id,
            'client_id'          =>$client->id,
            'evaluation'         =>$request->evaluation
        ]);

        return $this->apiResponse(new MealRatingResource($evaluation), 'The evaluation has been added successfully ', 200);
    }

    public function mealDetails($id){
        $meal=Meal::find($id);
        return $this->apiResponse(new MealResource($meal), 'Meal details ', 200);
    }

    public function getFavorite(Request $request){
        $client = $request->user('client_api');

         $meals=$client->meal_favorites()->paginate(20);
        return $this->apiResponse(MealResource::collection($meals), 'Favorite meals ', 200);
    }

    public function getRates($id){

        $meal=Meal::find($id);
        $rates=$meal->rates()->paginate(20);
        return $this->apiResponse(MealRatingResource::collection($rates), 'Meal rates ', 200);
    }





}
