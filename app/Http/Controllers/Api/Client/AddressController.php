<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Address\StoreAddressRequest;
use App\Http\Requests\Api\Client_App\Address\UpdateAddressRequest;
use App\Http\Resources\Api\Client_App\Address\AddressResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    use  ApiResponseTrait;
    public function index(Request $request)
    {
        $client = $request->user('client_api');
        $addresses = Address::where('client_id', $client->id)->orderBy('created_at', 'DESC')->get();
        return $this->apiResponse(AddressResource::collection($addresses), 'All Addresses ', 200);
    }


    public function store(StoreAddressRequest $request)
    {
        $client = $request->user('client_api');
        $address=Address::create([
            'client_id'    =>$client->id,
            'name'         =>$request->name,
            'location'     =>$request->location
        ]);
        return $this->apiResponse(new AddressResource($address), 'The address has been created successfully', 200);
    }



    public function update(UpdateAddressRequest $request, $id)
    {
        $client = $request->user('client_api');
        $address=Address::find($id);

        $address->update([
            'client_id'    =>$client->id,
            'name'         =>$request->name,
            'location'     =>$request->location
        ]);

        return $this->apiResponse(new AddressResource($address), 'The address has been updated successfully', 200);
    }


    public function destroy($id)
    {
        $address=Address::find($id);
        $address->delete();
        return $this->apiResponse(null, 'The address has been deleted successfully', 200);
    }
    public function SetAsMain(Request $request, $id)
    {
        $client = $request->user('client_api');
        $old=Address::where('status','1')->where('client_id',$client->id)->first();
        if($old){
            $old->update(['status' => '2']);
        }

        $address=Address::find($id);
        $address->update(['status' => '1']);

        return $this->apiResponse(null, 'The address has been set as main successfully', 200);
    }
    public function getMain(Request $request)
    {
        $client = $request->user('client_api');
        $main=Address::where('status','1')->where('client_id',$client->id)->first();

        return $this->apiResponse(new AddressResource($main), 'The main address of client', 200);
    }

}
