<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Client_App\Resturant\StoreRateRequest;

use App\Http\Resources\Api\Client_App\Resturant\ResturantRatingResource;

use App\Http\Resources\Api\Resturant_App\Resturant\ResturantResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\RatingReport;
use App\Models\Resturant;
use App\Models\ResturantFavorite;
use App\Models\ResturantRating;
use Illuminate\Http\Request;

class ResturantController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index()
    {
        $resturant = Resturant::paginate(20);
        return $this->apiResponse(ResturantResource::collection($resturant), 'All restaurant ', 200);
    }

    public function addToFavorite(Request $request,$id){
        $client = $request->user('client_api');
        $resturant=Resturant::find($id);

        $old_favorite=ResturantFavorite::where('resturant_id',$resturant->id)
                                       ->where('client_id',$client->id)->first();
        if($old_favorite){
            return $this->apiResponse(null, 'This resturant is already added to favorite', 200);
        }

        $client->resturant_favorites()->attach($resturant->id);

        return $this->apiResponse(null, 'Resturant successfully added to Favorite', 200);
    }

    public function removeFromFavorite(Request $request,$id){
        $client = $request->user('client_api');
        $resturant=Resturant::find($id);

        $client->resturant_favorites()->detach($resturant->id);

        return $this->apiResponse(null, 'Resturant successfully removed from Favorite', 200);
    }

    public function addEvaluation(StoreRateRequest $request,$id){
        $client = $request->user('client_api');
        $resturant=Resturant::find($id);
        $evaluation=ResturantRating::create([
            'resturant_id'       =>$resturant->id,
            'client_id'          =>$client->id,
            'evaluation'         =>$request->evaluation,
            'client_experience'  =>$request->client_experience
        ]);

        return $this->apiResponse(new ResturantRatingResource($evaluation), 'The evaluation has been added successfully ', 200);
    }

    public function getFavorite(Request $request){
        $client = $request->user('client_api');

        $meals=$client->resturant_favorites()->paginate(20);
        return $this->apiResponse(ResturantResource::collection($meals), 'Favorite resturants ', 200);
    }

    public function getRates($id){

        $resturant=Resturant::find($id);
        $rates=$resturant->rates()->paginate(20);
        $data = [
            'image' => $resturant->logo,
            'rates' =>ResturantRatingResource::collection($rates)
        ];
        return $this->apiResponse($data, 'Resturant rates ', 200);
    }

    public function addReport(Request $request,$id){
        $client = $request->user('client_api');
        $rate=ResturantRating::find($id);
        $report=RatingReport::create([
           'resturant_rating_id'   =>$rate->id,
           'client_id'             => $client->id
        ]);

        return $this->apiResponse(null, 'The report added successfully', 200);
    }



}
