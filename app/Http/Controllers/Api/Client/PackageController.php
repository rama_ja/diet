<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Client_App\Package\StoreRateRequest;
use App\Http\Resources\Api\Client_App\Package\PackageRatingResource;
use App\Http\Resources\Api\Resturant_App\Package\PackageResource;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Package;
use App\Models\PackageRating;
use App\Models\Resturant;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    use SaveImageTrait, ApiResponseTrait;

    public function index(Request $request,$id)
    {
        $resturant=Resturant::find($id);
        $packages = Package::where('resturant_id',$resturant->id)->paginate(20);
        return $this->apiResponse(PackageResource::collection($packages), 'All package ', 200);
    }
    public function addEvaluation(StoreRateRequest $request,$id){
        $client = $request->user('client_api');
        $package=Package::find($id);
        $evaluation=PackageRating::create([
            'package_id'         =>$package->id,
            'client_id'          =>$client->id,
            'evaluation'         =>$request->evaluation
        ]);

        return $this->apiResponse(new PackageRatingResource($evaluation), 'The evaluation has been added successfully ', 200);
    }

    public function packageDetails($id){
        $package=Package::find($id);
        return $this->apiResponse(new PackageResource($package), 'Package details ', 200);
    }
    public function getRates($id){

        $package=Package::find($id);
        $rates=PackageRating::where('package_id',$package->id)->paginate(20);

        return $this->apiResponse(PackageRatingResource::collection($rates), 'Package rates ', 200);
    }
}
