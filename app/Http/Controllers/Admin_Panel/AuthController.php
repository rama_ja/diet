<?php

namespace App\Http\Controllers\Admin_Panel;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\SaveImageTrait;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use SaveImageTrait,ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admin_api', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$token = auth('admin_api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }
    public function register(Request $request)
    {
            $admin = Admin::create([
                'name'              => $request->name,
                'email'             => $request->email,
                'password'          => Hash::make($request->password),
                ]);

        if($request->file('personal_image')) {
            $admin->update([
                'personal_image'  => $this->saveImage($request->file('personal_image'), 'Images\Admins', 400, 400),
            ]);
        }

        $token = auth('admin_api')->login($admin);

        return response()->json([
            'message'      => 'Admin successfully registered',
            'access_token' => $token,
            'Admin'    => $admin
        ], 201);
    }

    public function logout(Request $request)
    {
        if ($request->token) {
            $tokens = ClientDeviceToken::where('token', $request->token)->first();
            $tokens->delete();
        }
        auth('admin_api')->logout();
        return response()->json(['message' => 'Admin successfully signed out']);
    }

    public function refresh()
    {
        return $this->createNewToken(auth('admin_api')->refresh());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth('admin_api')->factory()->getTTL() * 60,
            'admin'        => auth('admin_api')->user()
        ]);
    }

}
