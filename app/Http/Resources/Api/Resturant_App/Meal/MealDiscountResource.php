<?php

namespace App\Http\Resources\Api\Resturant_App\Meal;

use Illuminate\Http\Resources\Json\JsonResource;

class MealDiscountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'meal_id'             => $this->meal_id,
            'has_discount'        => $this->has_discount,
            'discount_type'       => $this->discount_type,
            'discount_value'      => $this->discount_value,
            'discount_start_date' => $this->discount_start_date,
            'discount_end_date'   => $this->discount_end_date,
        ];
    }
}
