<?php

namespace App\Http\Resources\Api\Resturant_App\Representative;

use App\Http\Resources\Api\Representative_App\VehicleImagesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RepresentativeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'resturant_id'        => $this->resturant_id,
            'name'                => $this->name,
            'email'               => $this->email,
            'mobile'              => $this->mobile,
            'city_id'             => $this->city_id,
            'personal_image'      => $this->personal_image,
            'vehicle_type'        => $this->vehicle_type,
            'vehicle_brand'       => $this->vehicle_brand,
            'type'                => $this->type,
            'vehicle_model'       => $this->vehicle_model,
            'license_image'       => $this->license_image,
            'form_image'          => $this->form_image,
            'vehicle_number'      => $this->vehicle_number,
            'status'              => $this->status,
            'images'              => $this->images ?  VehicleImagesResource::collection($this->images) : null,
        ];

    }
}
