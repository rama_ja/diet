<?php

namespace App\Http\Resources\Api\Resturant_App\Resturant;

use App\Http\Resources\Api\Client_App\Resturant\ResturantRatingResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ResturantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $islike=false;
        if($this->islike->count()!= 0){
            $islike=true;
        }


        return [
            'id'                  => $this->id,
            'name_ar'             => $this->translate('ar')->name,
            'name_en'             => $this->translate('en')->name,
            'email'               => $this->email,
            'mobile'              => $this->mobile,
            'city_id'             => $this->city_id,
            'latitude'            => $this->latitude,
            'longitude'           => $this->longitude,
            'status'              => $this->status,
            'language'            => $this->language,
            'description_ar'      => $this->translate('ar')->description,
            'description_en'      => $this->translate('en')->description,
            'logo'                => $this->logo,
            'main_image'          => $this->main_image,
            'commercial_record'   => $this->commercial_record,
            'municipal_licence'   => $this->municipal_licence,
            'images'              => $this->images ?  ResturantImagesResource::collection($this->images) : null,
            'working_hours'       => $this->workingHours ?  WorkingHoursResource::collection($this->workingHours) : null,
            'rates'               => $this->rates ?  ResturantRatingResource::collection($this->rates) : null,
            'isLike'              => $islike
        ];
    }
}
