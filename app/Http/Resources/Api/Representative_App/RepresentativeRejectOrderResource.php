<?php

namespace App\Http\Resources\Api\Representative_App;

use Illuminate\Http\Resources\Json\JsonResource;

class RepresentativeRejectOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'representative_id'     => $this->representative_id,
            'order_id'              => $this->order_id,
            'cause'                 => $this->cause
        ];
    }
}
