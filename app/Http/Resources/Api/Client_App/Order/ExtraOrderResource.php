<?php

namespace App\Http\Resources\Api\Client_App\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class ExtraOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'name_ar'             => $this->extra->translate('ar')->name,
            'name_en'             => $this->extra->translate('en')->name,
            'image'               => $this->extra->image,
            'quantity'            => $this->quantity,
        ];
    }
}
