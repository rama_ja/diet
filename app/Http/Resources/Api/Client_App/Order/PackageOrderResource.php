<?php

namespace App\Http\Resources\Api\Client_App\Order;

use App\Http\Resources\Api\Resturant_App\Package\PackageDetailsResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PackageOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'name_ar'             => $this->package->translate('ar')->name,
            'name_en'             => $this->package->translate('en')->name,
            'main_image'          => $this->package->main_image,
            'details'             => $this->order->detailChange ? DetailChangeOrderResource::collection($this->order->detailChange) : null,
            'quantity'            => $this->quantity,
        ];
    }
}
