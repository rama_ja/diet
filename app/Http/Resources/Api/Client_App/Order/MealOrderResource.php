<?php

namespace App\Http\Resources\Api\Client_App\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class MealOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'name_ar'             => $this->meal->translate('ar')->name,
            'name_en'             => $this->meal->translate('en')->name,
            'main_image'          => $this->meal->main_image,
            'quantity'            => $this->quantity,
        ];
    }
}
