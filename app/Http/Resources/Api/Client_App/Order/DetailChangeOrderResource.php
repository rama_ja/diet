<?php

namespace App\Http\Resources\Api\Client_App\Order;

use App\Http\Resources\Api\Resturant_App\Resturant\DayResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DetailChangeOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->package_detail->id,
            'day'                 => new DayResource($this->package_detail->day),
            'delivery_time'       => $this->delivery_time,
            'description_ar'      => $this->package_detail->translate('ar')->description,
            'description_en'      => $this->package_detail->translate('en')->description,
        ];
    }
}
