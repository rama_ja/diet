<?php

namespace App\Http\Resources\Api\Client_App\Order;

use App\Http\Resources\Api\Client_App\Address\AddressResource;
use App\Http\Resources\Api\Resturant_App\Coupon\CouponResource;
use App\Http\Resources\Api\Resturant_App\Extra\ExtraResource;
use App\Http\Resources\Api\Resturant_App\Meal\MealResource;
use App\Http\Resources\Api\Resturant_App\Package\PackageResource;
use App\Http\Resources\Api\Resturant_App\Representative\RepresentativeResource;
use App\Http\Resources\Api\Resturant_App\Resturant\ResturantResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                         => $this->id,
            'client_id'                  => $this->client_id,
            'representative_id'          => $this->representative ?  new RepresentativeResource($this->representative) : null,
            'address_id'                 => $this->address ?  new AddressResource($this->address) : null,
            'resturant_id'               => $this->resturant ?  new ResturantResource($this->resturant) : null,
            'coupon_id'                  => $this->coupon ?  new CouponResource($this->coupon) : null,
            'payment_method'             => $this->payment_method,
            'delivery_commission'        => $this->delivery_commission,
            'order_details'              => $this->order_details,
            'total_price'                => $this->total_price,
            'has_discount'               => $this->has_discount,
            'discount_type'              => $this->discount_type,
            'discount_value'             => $this->discount_value,
            'total'                      => $this->total,
            'status'                     => $this->status,
            'meals'                      => $this->mealOreders ? MealOrderResource::collection($this->mealOreders) : null,
            'extras'                     => $this->extraOreders ? ExtraOrderResource::collection($this->extraOreders) : null,
            'packages'                   => $this->packageOreders ? PackageOrderResource::collection($this->packageOreders) : null,
            'date'                       => $this->created_at
        ];

    }
}
