<?php

namespace App\Http\Resources\Api\Client_App\Package;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageRatingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'evaluation'   => $this->evaluation
        ];
    }
}
