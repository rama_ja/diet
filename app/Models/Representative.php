<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Representative extends  Authenticatable implements JWTSubject
{
    use HasFactory;
    protected $fillable = ['resturant_id',  'name','email', 'mobile', 'password', 'city_id', 'personal_image', 'vehicle_type', 'vehicle_brand', 'type','vehicle_model','license_image','form_image','status','vehicle_number'];


    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['email_verified_at' => 'datetime'];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    ##---------- Relationships ----------##

    /**
     * Get the images for the resturant.
     */
    public function images()
    {
        return $this->hasMany(VehicleImage::class);
    }


    public function rejectedOrder()
    {
        return $this->belongsToMany(Order::class,'representative_reject_orders','representative_id','order_id');
    }

}
