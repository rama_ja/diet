<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResturantRating extends Model
{
    use HasFactory;
    protected $fillable = ['resturant_id', 'client_id','evaluation','client_experience'];
}
