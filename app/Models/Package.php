<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Package extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['resturant_id', 'price', 'total_calories', 'carbohydrate', 'protein', 'fats', 'main_image', 'status'];

    public $translatedAttributes = ['name', 'description'];

    ##---------- Relationships ----------##

    /**
     * Get the images for the package.
     */
    public function images()
    {
        return $this->hasMany(PackageImage::class);
    }

    /**
     * Get the details for the package.
     */
    public function details()
    {
        return $this->hasMany(PackageDetails::class);
    }

    public function rates()
    {
        return $this->hasMany(PackageRating::class);
    }
}
