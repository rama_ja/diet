<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageRating extends Model
{
    use HasFactory;
    protected $fillable = ['package_id', 'client_id','evaluation'];
}
