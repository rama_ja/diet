<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Meal extends Model implements TranslatableContract
{
    use HasFactory, Translatable;


    protected $fillable = ['resturant_id', 'price', 'total_calories', 'carbohydrate', 'protein', 'fats', 'main_image', 'status'];

    public $translatedAttributes = ['name', 'description'];


    ##---------- Relationships ----------##

    /**
     * The extras that belong to the meal.
     */
    public function extras()
    {
        return $this->belongsToMany(Extra::class);
    }

    /**
     * Get the images for the meal.
     */
    public function images()
    {
        return $this->hasMany(MealImage::class);
    }

    /**
     * Get the discount associated with the meal.
     */
    public function discount()
    {
        return $this->hasOne(MealDiscount::class);
    }

    public function isLike()
    {
        $user=auth('client_api')->user();
        if($user){
            return  $like= $this->hasMany(MealFavorite::class)->where('client_id',$user->id);
        }
        return  $like= $this->hasMany(MealFavorite::class);
    }

    public function rates()
    {
        return $this->hasMany(MealRating::class);
    }


}
