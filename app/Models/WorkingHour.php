<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkingHour extends Model
{
    use HasFactory;

    protected $fillable = ['resturant_id', 'day_id', 'opening_hour', 'closing_hour'];


    ##---------- Relationships ----------##
    /**
     * Get the day that owns the Working_Hour.
     */
    public function day()
    {
        return $this->belongsTo(Day::class);
    }
}
