<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class PackageDetails extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['package_id', 'day_id', 'delivery_time'];

    public $translatedAttributes = ['description'];

    ##---------- Relationships ----------##

    /**
     * Get the package that owns the details.
     */
    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    /**
     * Get the day that owns the details.
     */
    public function day()
    {
        return $this->belongsTo(Day::class);
    }
}
