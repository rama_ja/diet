<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use function PHPUnit\Framework\isEmpty;

class Resturant extends Authenticatable implements JWTSubject, TranslatableContract
{
    use HasFactory, Notifiable, Translatable;

    protected $fillable = ['email', 'mobile', 'password', 'city_id', 'latitude', 'longitude', 'logo', 'main_image', 'commercial_record', 'municipal_licence', 'language', 'status'];

    public $translatedAttributes = ['name', 'description'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['email_verified_at' => 'datetime'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    ##---------- Relationships ----------##

    /**
     * Get the images for the resturant.
     */
    public function images()
    {
        return $this->hasMany(ResturantImage::class);
    }

    /**
     * Get the workingHours for the resturant.
     */
    public function workingHours()
    {
        return $this->hasMany(WorkingHour::class);
    }
    public function meals()
    {
        return $this->hasMany(Meal::class);
    }

    public function isLike()
    {
        $user=auth('client_api')->user();
        if($user){
            return  $like= $this->hasMany(ResturantFavorite::class)->where('client_id',$user->id);
        }

        return $like= $this->hasMany(ResturantFavorite::class);
    }
    public function rates()
    {
        return $this->hasMany(ResturantRating::class);
    }
}
