<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Client extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;
    protected $fillable = [ 'name','email', 'mobile', 'password', 'city_id', 'personal_image'];


    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['email_verified_at' => 'datetime'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function meal_favorites()
    {
        return $this->belongsToMany(Meal::class,'meal_favorites', 'client_id', 'meal_id');
    }

    public function resturant_favorites()
    {
        return $this->belongsToMany(Resturant::class,'resturant_favorites', 'client_id', 'resturant_id');
    }
    public function coupons()
    {
        return $this->belongsToMany(Coupon::class,'coupon_clients', 'client_id', 'coupon_id');
    }


}
