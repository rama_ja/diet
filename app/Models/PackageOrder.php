<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageOrder extends Model
{
    use HasFactory;
    protected $fillable = ['package_id', 'order_id','quantity'];

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
