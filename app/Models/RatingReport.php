<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RatingReport extends Model
{
    use HasFactory;
    protected $fillable = ['resturant_rating_id','client_id'];

}
