<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'ar' => ['name' => 'الرياض'],
                'en' => ['name' => 'Riyadh']
            ],
            [
                'ar' => ['name' => 'مكة المكرمة'],
                'en' => ['name' => 'Makkah']
            ],
            [
                'ar' => ['name' => 'الباحة'],
                'en' => ['name' => 'Al-Baha']
            ],
            [
                'ar' => ['name' => 'نجران'],
                'en' => ['name' => 'Najran']
            ],
            [
                'ar' => ['name' => 'المنطقة الشرقيه'],
                'en' => ['name' => 'Eastern']
            ],
            [
                'ar' => ['name' => 'المدينة [المنورة'],
                'en' => ['name' => 'Al-Medina']
            ],
            [
                'ar' => ['name' => 'الجوف'],
                'en' => ['name' => 'Al-Jouf']
            ],
            [
                'ar' => ['name' => 'جازان'],
                'en' => ['name' => 'Jazan']
            ],
            [
                'ar' => ['name' => 'القصيم'],
                'en' => ['name' => 'Qassim']
            ],
            [
                'ar' => ['name' => 'عسير'],
                'en' => ['name' => 'Aseer.']
            ],
            [
                'ar' => ['name' => 'تبوك'],
                'en' => ['name' => 'Tabouk']
            ],
            [
                'ar' => ['name' => 'حائل'],
                'en' => ['name' => 'Hail.']
            ],
            [
                'ar' => ['name' => 'الدوادمي'],
                'en' => ['name' => 'Dawadmi']
            ],
            [
                'ar' => ['name' => 'سليل'],
                'en' => ['name' => 'Saleel']
            ],
            [
                'ar' => ['name' => 'عفيف'],
                'en' => ['name' => 'Afeef']
            ],
            [
                'ar' => ['name' => 'الدرعية'],
                'en' => ['name' => 'Diriyah']
            ],
            [
                'ar' => ['name' => 'غات'],
                'en' => ['name' => 'Ghat']
            ],
            [
                'ar' => ['name' => 'الزفلي'],
                'en' => ['name' => 'Zulfi']
            ],
            [
                'ar' => ['name' => 'مارات'],
                'en' => ['name' => 'Marat']
            ],
            [
                'ar' => ['name' => 'الخرج'],
                'en' => ['name' => 'Kharj']
            ],
            [
                'ar' => ['name' => 'المجمعة'],
                'en' => ['name' => 'Majmaah']
            ],
            [
                'ar' => ['name' => 'صادق'],
                'en' => ['name' => 'Thadeq']
            ],
            [
                'ar' => ['name' => 'الشارقة'],
                'en' => ['name' => 'Shaqraa']
            ],
            [
                'ar' => ['name' => 'الطائف'],
                'en' => ['name' => 'Taef']
            ],
            [
                'ar' => ['name' => 'القطيف'],
                'en' => ['name' => 'Qateef']
            ],
            [
                'ar' => ['name' => 'الجبيل'],
                'en' => ['name' => 'Jubail']
            ],
            [
                'ar' => ['name' => 'البقيق'],
                'en' => ['name' => 'Beqaiq']
            ],
            [
                'ar' => ['name' => 'ينبع'],
                'en' => ['name' => 'Yanbu']
            ],
            [
                'ar' => ['name' => 'بدر'],
                'en' => ['name' => 'Badr']
            ],
            [
                'ar' => ['name' => 'أبها'],
                'en' => ['name' => 'Abha']
            ],
            [
                'ar' => ['name' => 'الدمام'],
                'en' => ['name' => 'Dammam']
            ],
            [
                'ar' => ['name' => 'جدة'],
                'en' => ['name' => 'jadda']
            ],
            [
                'ar' => ['name' => 'العليا'],
                'en' => ['name' => 'Al-Ulya']
            ],
        ];

        foreach ($cities as $city) {
            City::create($city);
        }
    }
}
