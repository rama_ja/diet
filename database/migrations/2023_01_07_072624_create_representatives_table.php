<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representatives', function (Blueprint $table) {
            $table->id();
            // The data of the 1 stage
            $table->foreignId('resturant_id')->nullable()->constrained('resturants')->onDelete('cascade');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('mobile')->unique();
            $table->string('password');
            $table->foreignId('city_id')->constrained('cities')->onDelete('cascade');
            $table->string('personal_image')->default('Images/Representatives/PersonalImage/avatar.png');

            // The data of the 2 stage
            $table->string('vehicle_type')->nullable();
            $table->string('vehicle_brand')->nullable();
            $table->string('type')->nullable();
            $table->string('vehicle_model')->nullable();
            $table->string('vehicle_number')->nullable();
//            $table->integer('manufacturing_year')->nullable();
            $table->string('license_image')->nullable();
            $table->string('form_image')->nullable();
            $table->boolean('status')->comment('1:accepted, 2:not accepted')->default('1');

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representatives');
    }
}
