<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('resturant_id')->constrained('resturants')->onDelete('cascade');
            $table->double('price');
            $table->double('total_calories');
            $table->double('carbohydrate');
            $table->double('protein');
            $table->double('fats');
            $table->string('main_image')->nullable();
            $table->boolean('status')->comment('1:available, 2:not available')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
    }
}
