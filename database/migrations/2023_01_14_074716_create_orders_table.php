<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')->constrained('clients')->onDelete('cascade');
            $table->foreignId('representative_id')->nullable()->constrained('representatives')->onDelete('cascade');
            $table->foreignId('address_id')->constrained('addresses')->onDelete('cascade');
            $table->foreignId('resturant_id')->constrained('resturants')->onDelete('cascade');
            $table->foreignId('coupon_id')->nullable()->constrained('coupons')->onDelete('cascade');
            $table->string('payment_method')->default('credit card');
            $table->integer('delivery_commission');
            $table->boolean('status')->comment('1 waiting for reply, 2 Order is being processed, 3 Order is being delivered, 4 have just arrived, 5 Order has been received, 6 rejected')->default('1');
            $table->text('order_details');
            $table->integer('total_price');
            $table->boolean('has_discount')->default('1')->comment('0:no discount, 1:has_discount');
            $table->boolean('discount_type')->default('0')->comment('0:Percentage, 1:Fixed amount')->nullable();
            $table->double('discount_value')->nullable();
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
