<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TermsTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('terms_id')->constrained('terms')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->longText('content');
            $table->unique(['terms_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms_translations');
    }
}
