<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AboutAppTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_app_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('about_app_id')->constrained('about_apps')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->longText('content');
            $table->unique(['about_app_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_app_translations');
    }
}
