<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>CORK Admin - Multipurpose Bootstrap Dashboard Template </title>
    <link rel="icon" type="image/x-icon" href="{{ asset('/src/assets/img/favicon.ico') }}"/>
    <link href="{{ asset('/css/light/loader.css') }} /public/css/light/loader.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/dark/loader.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('/js/loader.js') }}"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap.rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/light/plugins.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/dark/plugins.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('/css/apexcharts.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/light/dash_1.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/dark/dashboard/dash_1.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

</head>

{{--<head>--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">--}}
{{--    <title>CORK Admin - Multipurpose Bootstrap Dashboard Template </title>--}}
{{--    <link rel="icon" type="image/x-icon" href="../src/assets/img/favicon.ico"/>--}}
{{--    <link href="../layouts/vertical-light-menu/css/light/loader.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="../layouts/vertical-light-menu/css/dark/loader.css" rel="stylesheet" type="text/css" />--}}
{{--    <script src="../layouts/vertical-light-menu/loader.js"></script>--}}

{{--    <!-- BEGIN GLOBAL MANDATORY STYLES -->--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">--}}
{{--    <link href="../src/bootstrap/css/bootstrap.rtl.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="../layouts/vertical-light-menu/css/light/plugins.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="../layouts/vertical-light-menu/css/dark/plugins.css" rel="stylesheet" type="text/css" />--}}
{{--    <!-- END GLOBAL MANDATORY STYLES -->--}}

{{--    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->--}}
{{--    <link href="../src/plugins/src/apex/apexcharts.css" rel="stylesheet" type="text/css">--}}
{{--    <link href="../src/assets/css/light/dashboard/dash_1.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="../src/assets/css/dark/dashboard/dash_1.css" rel="stylesheet" type="text/css" />--}}
{{--    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->--}}

{{--</head>--}}
